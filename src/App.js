import React from "react";
import { Route, Routes } from "react-router-dom";
import Layout from "./layout";
import Home from "./pages/Home";
import Posts from "./pages/posts/Posts";
import ToDos from "./pages/todos/ToDos";
import Error from "./error/Error";
import Photos from "./pages/photos/Photos";


const App = () => {
  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route index element={<Home />} />
        <Route path="/photos" element={<Photos/>} />
        <Route path="/todos" element={<ToDos />} />
        <Route path="/posts" element={<Posts />} />
      </Route>
      <Route path="*" element={<Error />} />
    </Routes>
  );
};

export default App;
