import React from 'react';

const ErrorPage = () => {
  return (
    <div className="flex flex-col items-center justify-center h-screen">
      <h1 className="text-4xl font-bold mb-4 text-red-800">Oops!</h1>
      <p className="text-lg text-gray-600">
        We're sorry, but an error occurred.
      </p>
    </div>
  );
};

export default ErrorPage;
