import React from "react";
import { Link } from "react-router-dom";
import {ImHome} from 'react-icons/im'

const Header = () => {
  return (
    <div className="container mx-auto ">
      <nav className="bg-gray-800">
        <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
          <div className="flex items-center justify-between h-16">
            <div className="flex items-center">
              <Link to="/" className="text-white text-xl font-bold flex items-center gap-2">
               <ImHome/> Home
              </Link>
            </div>
            <div className="flex">
              <Link
                to="/photos"
                className="text-gray-300 hover:bg-gray-700 px-3 py-2 rounded-md text-sm font-medium"
              >
                Photos
              </Link>
              <Link
                to="/todos"
                className="text-gray-300 hover:bg-gray-700 px-3 py-2 rounded-md text-sm font-medium"
              >
                Todos
              </Link>
              <Link
                to="/posts"
                className="text-white hover:bg-gray-700 px-3 py-2 rounded-md text-sm font-medium"
              >
                Posts
              </Link>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Header;
