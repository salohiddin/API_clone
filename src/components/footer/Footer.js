import React from 'react';

const Footer = () => {
  return (
    <div className='container mx-auto'>
   <footer className="bg-gray-500 py-4">
      <div className="container mx-auto text-center">
        <p className="text-gray-600 text-sm">
          &copy; {new Date().getFullYear()} My Website. All rights reserved.
        </p>
      </div>
    </footer>
    </div> 
  );
};

export default Footer;
