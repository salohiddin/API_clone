import React, { useState, useEffect } from "react";
import axios from "axios";
import { BsPencil, BsTrash, BsStar } from "react-icons/bs";
import {
  Checkbox,
  FormControlLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import {
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  Button,
} from "@mui/material";
import { Close } from "@mui/icons-material";

function App() {
  const [albums, setAlbums] = useState([]);
  const [filteredAlbums, setFilteredAlbums] = useState([]);
  const [selectedAlbumIds, setSelectedAlbumIds] = useState([]);
  const [loading, setLoading] = useState(true);
  const [openAlbumDialog, setOpenAlbumDialog] = useState(false);
  const [selectedAlbum, setSelectedAlbum] = useState(null);

  const [perPage, setPerPage] = useState(10);
  const [searchTerm, setSearchTerm] = useState("");
  const [sortBy, setSortBy] = useState("");

  useEffect(() => {
    axios
      .get("https://jsonplaceholder.typicode.com/albums")
      .then((response) => {
        setAlbums(response.data);
        setFilteredAlbums(response.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
      });
  }, []);

  useEffect(() => {
    applyFilters();
  }, [searchTerm, sortBy, albums]);

  const handlePerPageChange = (event) => {
    setPerPage(event.target.value);
  };

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
  };

  const handleSortByChange = (event) => {
    setSortBy(event.target.value);
  };

  const applyFilters = () => {
    let filtered = albums;

    if (searchTerm) {
      filtered = filtered.filter((album) =>
        album.title.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }

    if (sortBy) {
      filtered.sort((a, b) => {
        if (sortBy === "asc") {
          return a.title.localeCompare(b.title);
        } else if (sortBy === "desc") {
          return b.title.localeCompare(a.title);
        }
        return 0;
      });
    }

    setFilteredAlbums(filtered);
  };

  const handleEditAlbum = (album) => {
    console.log("Edit album:", album);
  };

  const handleDeleteAlbum = (album) => {
    console.log("Delete album:", album);
  };

  const handleFavoriteAlbum = (album) => {
    console.log("Favorite album:", album);
  };

  const handleCheckboxChange = (event, albumId) => {
    if (event.target.checked) {
      setSelectedAlbumIds([...selectedAlbumIds, albumId]);
    } else {
      setSelectedAlbumIds(selectedAlbumIds.filter((id) => id !== albumId));
    }
  };

  const handleOpenAlbumDialog = (album) => {
    setSelectedAlbum(album);
    setOpenAlbumDialog(true);
  };

  const handleCloseAlbumDialog = () => {
    setOpenAlbumDialog(false);
  };

  return (
    <div className="container mx-auto p-4">
      <Typography variant="h5" component="h2" className="mb-4">
        Photos
      </Typography>

      <div className="flex items-center mb-4">
        <TextField
          label="Search"
          value={searchTerm}
          onChange={handleSearchChange}
          variant="outlined"
          size="small"
          className="mr-4"
        />

        <FormControlLabel
          control={
            <Select
              value={perPage}
              onChange={handlePerPageChange}
              variant="outlined"
              size="small"
            >
              <MenuItem value={10}>10</MenuItem>
              <MenuItem value={20}>20</MenuItem>
              <MenuItem value={50}>50</MenuItem>
              <MenuItem value={100}>100</MenuItem>
              <MenuItem value={filteredAlbums.length}>All</MenuItem>
            </Select>
          }
          label="Per Page"
          className="mr-4"
        />

        <FormControlLabel
          control={
            <Select
              value={sortBy}
              onChange={handleSortByChange}
              variant="outlined"
              size="small"
            >
              <MenuItem value="">None</MenuItem>
              <MenuItem value="asc">A-Z</MenuItem>
              <MenuItem value="desc">Z-A</MenuItem>
            </Select>
          }
          label="Sort By"
        />
      </div>

      {loading ? (
        <Typography variant="body1">Loading albums...</Typography>
      ) : (
        <Grid container spacing={2}>
          {filteredAlbums.slice(0, perPage).map((album) => (
            <Grid item xs={12} md={6} key={album.id} className="sm:block">
              <div className="p-4 border border-gray-300 rounded-md">
                <div className="flex items-center justify-between mb-2">
                  <Typography variant="subtitle1">{album.title}</Typography>
                  <div>
                    <Button size="small" onClick={() => handleEditAlbum(album)}>
                      <BsPencil />
                    </Button>
                    <Button
                      size="small"
                      onClick={() => handleDeleteAlbum(album)}
                    >
                      <BsTrash />
                    </Button>
                    <Button
                      size="small"
                      onClick={() => handleFavoriteAlbum(album)}
                    >
                      <BsStar />
                    </Button>
                    <Checkbox
                      size="small"
                      checked={selectedAlbumIds.includes(album.id)}
                      onChange={(event) =>
                        handleCheckboxChange(event, album.id)
                      }
                    />
                  </div>
                </div>
                <Typography variant="caption" color="textSecondary">
                  {album.userId}
                </Typography>
                <button
                  className="mt-2 bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600"
                  onClick={() => handleOpenAlbumDialog(album)}
                >
                  View Photos
                </button>
              </div>
            </Grid>
          ))}
        </Grid>
      )}

      <Dialog
        open={openAlbumDialog}
        onClose={handleCloseAlbumDialog}
        maxWidth="md"
      >
        <DialogTitle>{selectedAlbum && selectedAlbum.title}</DialogTitle>
        <DialogContent>
          {selectedAlbum && (
            <Grid container spacing={2}>
              {selectedAlbum.photos.map((photo) => (
                <Grid item xs={6} md={4} key={photo.id}>
                  <div className="relative">
                    <img
                      src={photo.thumbnailUrl}
                      alt={photo.title}
                      className="w-full h-auto"
                    />
                    <div className="absolute top-0 right-0 p-2">
                      <Button size="small" onClick={handleCloseAlbumDialog}>
                        <Close />
                      </Button>
                    </div>
                  </div>
                  <Typography variant="caption" color="textSecondary">
                    {photo.title}
                  </Typography>
                </Grid>
              ))}
            </Grid>
          )}
        </DialogContent>
      </Dialog>
    </div>
  );
}

export default App;
