import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { AiFillDelete, AiOutlineComment, AiOutlineEdit } from 'react-icons/ai';
import { MdOutlineFavorite, MdFavorite } from 'react-icons/md';
import {
  Pagination,
  FormControl,
  Select,
  MenuItem,
  Checkbox,
  Button,
} from '@mui/material';

const useLocalStorage = (key, initialValue) => {
  const [value, setValue] = useState(() => {
    const storedValue = localStorage.getItem(key);
    return storedValue ? JSON.parse(storedValue) : initialValue;
  });

  useEffect(() => {
    localStorage.setItem(key, JSON.stringify(value));
  }, [key, value]);

  return [value, setValue];
};

const Posts = () => {
  const [posts, setPosts] = useState([]);
  const [selectedPosts, setSelectedPosts] = useLocalStorage('selectedPosts', []);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  useEffect(() => {
    fetchPosts();
  }, []);

  const fetchPosts = async () => {
    try {
      const response = await axios.get('https://jsonplaceholder.typicode.com/posts');
      setPosts(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };

  const handlePostsPerPageChange = (event) => {
    setPostsPerPage(event.target.value);
    setCurrentPage(1);
  };

  const handlePostSelect = (postId) => {
    if (selectedPosts.includes(postId)) {
      setSelectedPosts(selectedPosts.filter((id) => id !== postId));
    } else {
      setSelectedPosts([...selectedPosts, postId]);
    }
  };

  const handleDeleteSelectedPosts = () => {
    if (window.confirm('Are you sure you want to delete the selected posts?')) {
      const updatedPosts = posts.filter((post) => !selectedPosts.includes(post.id));
      setPosts(updatedPosts);
      setSelectedPosts([]);
    }
  };

  const handleToggleFavorite = (postId) => {
    setPosts((prevPosts) => {
      const updatedPosts = prevPosts.map((post) => {
        if (post.id === postId) {
          return { ...post, favorite: !post.favorite };
        }
        return post;
      });
      return updatedPosts;
    });
  };

  const handleEditPost = (postId, newContent) => {
    setPosts((prevPosts) => {
      const updatedPosts = prevPosts.map((post) => {
        if (post.id === postId) {
          return { ...post, content: newContent };
        }
        return post;
      });
      return updatedPosts;
    });
  };

  const handleAddComment = (postId, comment) => {
    setPosts((prevPosts) => {
      const updatedPosts = prevPosts.map((post) => {
        if (post.id === postId) {
          return { ...post, comments: [...post.comments, comment] };
        }
        return post;
      });
      return updatedPosts;
    });
  };

  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);

  return (
    <div className="container mx-auto py-8">
      <div className="flex justify-between mb-4">
        <FormControl variant="standard" sx={{ minWidth: 120 }}>
          <Select value={postsPerPage} onChange={handlePostsPerPageChange}>
            <MenuItem value={10}>10 per page</MenuItem>
            <MenuItem value={20}>20 per page</MenuItem>
            <MenuItem value={50}>50 per page</MenuItem>
            <MenuItem value={100}>100 per page</MenuItem>
            <MenuItem value={posts.length}>All</MenuItem>
          </Select>
        </FormControl>
        {selectedPosts.length > 0 && (
          <Button variant="contained" onClick={handleDeleteSelectedPosts}>
            <AiFillDelete />
          </Button>
        )}
      </div>
      {currentPosts.map((post) => (
        <div
          key={post.id}
          className="border border-gray-200 rounded p-4 mb-4 flex items-center"
        >
          <Checkbox
            checked={selectedPosts.includes(post.id)}
            onChange={() => handlePostSelect(post.id)}
          />
          <div className="flex-grow">
            <h2 className="text-xl font-bold sm:text-lg">{post.title}</h2>
            <p>Author: {post.author}</p>
            <p>{post.content}</p>
            <div className="flex">
              <Button
                variant="contained"
                onClick={() => handleToggleFavorite(post.id)}
              >
                {post.favorite ? <MdFavorite /> : <MdOutlineFavorite />}
              </Button>
              <Button variant="contained" onClick={() => handleAddComment(post.id, 'New Comment')}>
                <AiOutlineComment />
              </Button>
              <Button onClick={() => handleEditPost(post.id, 'Updated Content')}>
                <AiOutlineEdit />
              </Button>
              <Button>
                <AiFillDelete />
              </Button>
            </div>
          </div>
        </div>
      ))}
      <Pagination
        count={Math.ceil(posts.length / postsPerPage)}
        page={currentPage}
        onChange={handlePageChange}
        variant="outlined"
        shape="rounded"
        className="mt-4"
      />
    </div>
  );
};

export default Posts;
