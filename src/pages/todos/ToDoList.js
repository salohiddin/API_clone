import React from 'react';
import { BiEdit, BiTrash } from 'react-icons/bi';
import Checkbox from '@mui/material/Checkbox';

const TodoList = ({
  todos,
  filter,
  onToggleCompleted,
  onEditTodo,
  onSelectTodo,
  selectedTodos,
}) => {
  const filteredTodos = filterTodos(todos, filter);

  return (
    <ul className="todo-list">
      {filteredTodos.map((todo) => (
        <li
          key={todo.id}
          className={`todo-item flex justify-between items-center mb-2 p-2 border rounded ${
            todo.completed ? 'bg-gray-300' : ''
          }`}
        >
          <div className="flex items-center">
            <Checkbox
              checked={selectedTodos.includes(todo.id)}
              onChange={() => onSelectTodo(todo.id)}
            />
            <span
              className={`ml-2 cursor-pointer ${
                todo.completed ? 'line-through' : ''
              }`}
              onClick={() => onToggleCompleted(todo.id)}
            >
              {todo.title}
            </span>
          </div>
          <div>
            <BiEdit
              className="text-blue-500 cursor-pointer mr-2"
              onClick={() => onEditTodo(todo)}
            />
            <BiTrash
              className="text-red-500 cursor-pointer"
              onClick={() => onEditTodo(todo.id)}
            />
          </div>
        </li>
      ))}
    </ul>
  );
};

const filterTodos = (todos, filter) => {
  switch (filter) {
    case 'completed':
      return todos.filter((todo) => todo.completed);
    case 'incomplete':
      return todos.filter((todo) => !todo.completed);
    default:
      return todos;
  }
};

export default TodoList;
