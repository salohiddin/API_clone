import React, { useState, useEffect } from "react";
import TodoModal from "./TodoModal";
import TodoList from "./ToDoList";
import {
  Modal,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Button,
} from "@mui/material";


const API_URL = "https://jsonplaceholder.typicode.com/todos";

const App = () => {
  const [todos, setTodos] = useState([]);
  const [filter, setFilter] = useState("all");
  const [modalOpen, setModalOpen] = useState(false);
  const [newTodo, setNewTodo] = useState("");
  const [editingTodo, setEditingTodo] = useState(null);
  const [selectedTodos, setSelectedTodos] = useState([]);

  useEffect(() => {
    fetch(API_URL)
      .then((response) => response.json())
      .then((data) => setTodos(data));
  }, []);

  const handleFilterChange = (event) => {
    setFilter(event.target.value);
  };

  const handleAddTodo = () => {
    if (newTodo.trim() !== "") {
      const newTask = {
        id: Date.now(),
        title: newTodo,
        completed: false,
      };
      setTodos([...todos, newTask]);
      setNewTodo("");
    }
  };

  const handleUpdateTodo = (id, updatedTodo) => {
    setTodos(
      todos.map((todo) => (todo.id === id ? { ...todo, ...updatedTodo } : todo))
    );
  };

  const handleDeleteTodo = (id) => {
    setTodos(todos.filter((todo) => todo.id !== id));
  };

  const handleToggleCompleted = (id) => {
    setTodos(
      todos.map((todo) =>
        todo.id === id ? { ...todo, completed: !todo.completed } : todo
      )
    );
  };

  const handleEditTodo = (todo) => {
    setEditingTodo(todo);
    setModalOpen(true);
  };

  const handleSaveTodo = () => {
    setModalOpen(false);
    setEditingTodo(null);
  };

  const handleCancelEdit = () => {
    setModalOpen(false);
    setEditingTodo(null);
  };

  const handleSelectTodo = (id) => {
    const isSelected = selectedTodos.includes(id);
    if (isSelected) {
      setSelectedTodos(selectedTodos.filter((todoId) => todoId !== id));
    } else {
      setSelectedTodos([...selectedTodos, id]);
    }
  };

  const handleSelectAllTodos = () => {
    if (selectedTodos.length === todos.length) {
      setSelectedTodos([]);
    } else {
      const allTodoIds = todos.map((todo) => todo.id);
      setSelectedTodos(allTodoIds);
    }
  };

  const handleDeleteSelectedTodos = () => {
    const updatedTodos = todos.filter(
      (todo) => !selectedTodos.includes(todo.id)
    );
    setTodos(updatedTodos);
    setSelectedTodos([]);
  };

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-center">Todo List</h1>

      <div className="flex items-center mb-4">
        <FormControl className="w-1/2 mr-2">
          <InputLabel id="filter-label">Filter</InputLabel>
          <Select
            labelId="filter-label"
            id="filter"
            value={filter}
            onChange={handleFilterChange}
            className="w-full"
          >
            <MenuItem value="all">All</MenuItem>
            <MenuItem value="completed">Completed</MenuItem>
            <MenuItem value="incomplete">Incomplete</MenuItem>
          </Select>
        </FormControl>

        <Button
          variant="contained"
          color="primary"
          onClick={() => setModalOpen(true)}
        >
          Add Todo
        </Button>
      </div>

      <TodoList
        todos={todos}
        filter={filter}
        onToggleCompleted={handleToggleCompleted}
        onEditTodo={handleEditTodo}
        onSelectTodo={handleSelectTodo}
        selectedTodos={selectedTodos}
      />

      <Modal open={modalOpen} onClose={handleCancelEdit} center>
        <TodoModal
          newTodo={newTodo}
          setNewTodo={setNewTodo}
          editingTodo={editingTodo}
          onSaveTodo={handleSaveTodo}
          onDeleteTodo={handleDeleteTodo}
        />
      </Modal>

      {selectedTodos.length > 0 && (
        <div className="flex justify-end mt-4">
          <Button
            variant="contained"
            color="secondary"
            onClick={handleDeleteSelectedTodos}
          >
            Delete Selected
          </Button>
        </div>
      )}
    </div>
  );
};

export default App;
