import React from 'react';
import { Button, TextField } from '@mui/material';

const TodoModal = ({
  newTodo,
  setNewTodo,
  editingTodo,
  onSaveTodo,
  onDeleteTodo,
}) => {
  const handleSave = () => {
    onSaveTodo();
  };

  const handleDelete = () => {
    onDeleteTodo();
  };

  return (
    <div className='container mx-auto'>
      <h2>{editingTodo ? 'Edit Todo' : 'Add Todo'}</h2>

      <TextField
        label="Todo"
        variant="outlined"
        fullWidth
        value={newTodo}
        onChange={(e) => setNewTodo(e.target.value)}
        className="mb-2"
      />

      <div className="flex justify-end">
        <Button
          variant="contained"
          color="primary"
          onClick={handleSave}
          className="mr-2"
        >
          Save
        </Button>
        {editingTodo && (
          <Button
            variant="contained"
            color="secondary"
            onClick={handleDelete}
          >
            Delete
          </Button>
        )}
      </div>
    </div>
  );
};

export default TodoModal;
